# Brutal Prog

This is a test repository of using Drupal 9 for the Brutal Prog site using composer core-recommended

## Getting Started

* Primary development branch: develop
* Local environments: Lando
* Local drush alias: @my-project.local
* Local site URL: https://brutal-prog.lndo.site/

## Steps to install
1.  `lando start`
2.  `lando composer install`
3.  `lando drush site:install` Note: you can set the database credentials to whatever you want, but the database host needs to be `database`
4.  replace this line to end of /web/sites/default/settings.php:
    `$settings['config_sync_directory'] = "/app/config";`
5. `lando db-import initial-db.sql.gz`
6. `lando drush cr`
7. `lando drush cim`
8. `lando drush cr`

## Admin password

The initial admin account is admin/admin. If you do not import an newer version of the site's database after installation, you should probably change the administrator password.

## Resources

* [Lando Installation Instructions](https://docs.lando.dev/config/drupal9.html)
* [Drupal 9 Installation Instructions](https://www.drupal.org/docs/develop/using-composer/using-composer-to-install-drupal-and-manage-dependencies)
* [GitLab](https://gitlab.com/phil-plencner-hl/brutal-prog)


